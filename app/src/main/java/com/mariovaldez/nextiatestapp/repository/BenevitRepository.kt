package com.mariovaldez.nextiatestapp.repository

import com.mariovaldez.nextiatestapp.data.local.NextiaDao
import com.mariovaldez.nextiatestapp.data.local.NextiaItem
import com.mariovaldez.nextiatestapp.data.network.NextiaAPI
import javax.inject.Inject

class BenevitRepository @Inject() constructor(
    private val api: NextiaAPI,
    private val dao: NextiaDao
) {
    fun getWallets() = api.getWallets()

    fun getBenevits(auth:String) = api.getBenevits(auth)

    suspend fun getAuth() = dao.getNextiaItem()
}