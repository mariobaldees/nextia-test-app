package com.mariovaldez.nextiatestapp.repository

import com.mariovaldez.nextiatestapp.data.local.NextiaDao
import com.mariovaldez.nextiatestapp.data.network.NextiaAPI
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MainRepository @Inject constructor(
    private val api: NextiaAPI,
    private val dao: NextiaDao
)  {

    suspend fun signout() = dao.deleteNextiaItem()
}