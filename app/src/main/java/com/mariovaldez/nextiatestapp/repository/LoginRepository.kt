package com.mariovaldez.nextiatestapp.repository

import com.mariovaldez.nextiatestapp.data.local.NextiaDao
import com.mariovaldez.nextiatestapp.data.local.NextiaItem
import com.mariovaldez.nextiatestapp.data.network.NextiaAPI
import com.mariovaldez.nextiatestapp.data.remote.request.LoginRequest
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class LoginRepository @Inject constructor(
    private val api: NextiaAPI,
    private val dao: NextiaDao
) {

    fun login(login:LoginRequest) = api.login(login)

    suspend fun insertAuth(item: NextiaItem) = dao.insertNextiaItem(item)

}