package com.mariovaldez.nextiatestapp.di

import android.content.Context
import androidx.room.Room
import com.google.gson.GsonBuilder
import com.mariovaldez.nextiatestapp.data.local.NextiaItemDatabase
import com.mariovaldez.nextiatestapp.data.network.NextiaAPI
import com.mariovaldez.nextiatestapp.other.Constants
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun providesMovieItemDatabase(
        @ApplicationContext context: Context
    ) = Room.databaseBuilder(context, NextiaItemDatabase::class.java,Constants.DATABASE_NAME).build()

    @Singleton
    @Provides
    fun providesMoviesDao(database: NextiaItemDatabase) = database.nextiaDao()


    @Singleton
    @Provides
    fun providesMoviesAPI(retrofit: Retrofit):NextiaAPI{
        return retrofit.create(NextiaAPI::class.java)
    }

    @Singleton
    @Provides
    fun providesRetrofit(): Retrofit {
        val gsonBuilder = GsonBuilder()
        gsonBuilder.serializeNulls()
        val gson = gsonBuilder.create()
        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(Constants.URL)
            .build()
    }
}