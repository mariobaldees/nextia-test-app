package com.mariovaldez.nextiatestapp.adapter

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.mariovaldez.nextiatestapp.R
import com.mariovaldez.nextiatestapp.data.remote.model.Benevit
import com.mariovaldez.nextiatestapp.data.remote.response.WalletResponse
import com.mariovaldez.nextiatestapp.databinding.ItemBenevitBinding

class BevenitAdapter : RecyclerView.Adapter<BevenitAdapter.BenevitViewHolder>() {

    inner class BenevitViewHolder(val binding: ItemBenevitBinding):RecyclerView.ViewHolder(binding.root){}
    private val diffCallback = object  : DiffUtil.ItemCallback<Benevit>(){
        override fun areItemsTheSame(oldItem: Benevit, newItem: Benevit): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Benevit, newItem: Benevit): Boolean {
            return oldItem == newItem
        }

    }
    private val differ = AsyncListDiffer(this,diffCallback)
    lateinit var context: Context
    var benevitItems: List<Benevit>
        get() = differ.currentList
        set(value) {
            differ.submitList(value)
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BenevitViewHolder {
       context = parent.context
        val view = ItemBenevitBinding.inflate(LayoutInflater.from(context),parent,false)
        return BenevitViewHolder(view)
    }

    override fun onBindViewHolder(holder: BenevitViewHolder, position: Int) {
       val currentBenevit = benevitItems[position]
        holder.setIsRecyclable(false);
        holder.binding.apply {
            if(currentBenevit.isLocked){
                cardUnlocked.visibility = View.GONE
                cardLocked.visibility = View.VISIBLE
                Glide.with(context)
                    .load(currentBenevit.vector_full_path)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .dontAnimate()
                    .into(benevitImageLocked)
            }else{
                cardUnlocked.visibility = View.VISIBLE
                cardLocked.visibility = View.GONE
                println(currentBenevit.primary_color)
                if(currentBenevit.primary_color.length == 7){
                    lyImageUnlocked.setBackgroundColor(Color.parseColor(currentBenevit.primary_color))
                }
                Glide.with(context)
                    .load(currentBenevit.vector_full_path)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .dontAnimate()
                    .into(benevitImageUnlocked)
                txtTerritorieNameUnlocked.text = currentBenevit.territories.get(0).name
                txtTitleBenevitUnlocked.text = currentBenevit.title
                txtDurationUnlocked.text = currentBenevit.duration
            }
        }
    }

    override fun getItemCount(): Int {
        return benevitItems.size
    }
}