package com.mariovaldez.nextiatestapp.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mariovaldez.nextiatestapp.data.remote.model.Wallet
import com.mariovaldez.nextiatestapp.data.remote.response.WalletResponse
import com.mariovaldez.nextiatestapp.databinding.ItemWalletBinding

class WalletAdapter : RecyclerView.Adapter<WalletAdapter.WalletViewHolder>(){

    lateinit var adapterBenevit: BevenitAdapter

    inner class WalletViewHolder(val binding: ItemWalletBinding):RecyclerView.ViewHolder(binding.root) {
    }
    private val diffCallback = object  : DiffUtil.ItemCallback<WalletResponse>(){
        override fun areItemsTheSame(oldItem: WalletResponse, newItem: WalletResponse): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: WalletResponse, newItem: WalletResponse): Boolean {
            return oldItem == newItem
        }

    }

    private val differ = AsyncListDiffer(this,diffCallback)
    lateinit var context:Context
    var walletItems: List<WalletResponse>
        get() = differ.currentList
        set(value) {
            differ.submitList(value)
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WalletViewHolder {
       context = parent.context
        return WalletViewHolder(ItemWalletBinding.inflate(LayoutInflater.from(context),parent,false))
    }

    override fun onBindViewHolder(holder: WalletViewHolder, position: Int) {
        val currentWallet = walletItems[position]

        holder.binding.apply {
            titleWallet.text = currentWallet.display_text
            adapterBenevit = BevenitAdapter()

            rvBenevits.apply {
                adapter = adapterBenevit
                adapter!!.notifyDataSetChanged()
                layoutManager = LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false)
                setHasFixedSize(true)

            }
            adapterBenevit.benevitItems = currentWallet.benevits

        }

    }

    override fun getItemCount(): Int {
        return walletItems.size
    }
}