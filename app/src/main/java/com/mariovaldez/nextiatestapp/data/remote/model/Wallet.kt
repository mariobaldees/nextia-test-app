package com.mariovaldez.nextiatestapp.data.remote.model

import java.io.Serializable

data class Wallet(
    val id:Int,
    val name: String,
    val description : String,
    val display_text: String,
    val icon: String,
    val path: String,
    val primary_color: String,
    val secondary_color: String,
    val created_at: String,
    val updated_at: String,
    val display_index: Int,
    val avatar: String,
    val mobile_cover_url: String,
    val desktop_cover_url: String,
    val max_level: Int,
    val max_level_phase_2:Int,
    val max_level_phase_3:Int,
    val max_level_phase_4:Int,
    val max_level_phase_5:Int
    ):Serializable
