package com.mariovaldez.nextiatestapp.data.remote.response

import com.mariovaldez.nextiatestapp.data.remote.model.Member

data class LoginResponse(
    private val id: Int,
    private val email: String,
    private val role: String,
    private val member: Member,
    private val sign_in_count: Int
)
