package com.mariovaldez.nextiatestapp.data.remote.model

data class Territories(
    val id: Int,
    val name: String,
    val clave: String,
    val created_at: String,
    val updated_at: String
)
