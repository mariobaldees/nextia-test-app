package com.mariovaldez.nextiatestapp.data.local

import androidx.room.*

@Dao
interface NextiaDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertNextiaItem(nextiaItem: NextiaItem)

    @Query("SELECT * FROM user_auth WHERE id == 1")
    suspend fun getNextiaItem():NextiaItem

    @Query("DELETE FROM user_auth WHERE id == 1")
    suspend fun deleteNextiaItem()
}