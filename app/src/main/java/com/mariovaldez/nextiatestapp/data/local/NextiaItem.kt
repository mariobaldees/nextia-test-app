package com.mariovaldez.nextiatestapp.data.local

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "user_auth")
data class NextiaItem(
    @PrimaryKey(autoGenerate = false)
    val id : Int,
    val auth: String
)
