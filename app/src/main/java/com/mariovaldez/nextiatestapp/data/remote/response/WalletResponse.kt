package com.mariovaldez.nextiatestapp.data.remote.response

import com.mariovaldez.nextiatestapp.data.remote.model.Benevit
import java.io.Serializable

data class WalletResponse(
    val id:Int,
    val display_index:Int,
    val display_text: String,
    val icon: String,
    val path: String,
    val max_level: Int,
    val avatar: String,
    val name: String,
    val benevit_count:Int,
    val mobile_cover_url: String,
    val desktop_cover_url: String,
    val member_level: Int,
    val primary_color: String,
    val max_level_phase_2: Int,
    val max_level_phase_3: Int,
    val max_level_phase_4: Int,
    val max_level_phase_5: Int,
    var benevits : List<Benevit>
):Serializable