package com.mariovaldez.nextiatestapp.data.local

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(
    entities = [NextiaItem::class],
    version = 1
)
abstract class NextiaItemDatabase : RoomDatabase() {

    abstract fun nextiaDao(): NextiaDao
}