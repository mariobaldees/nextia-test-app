package com.mariovaldez.nextiatestapp.data.remote.response

import com.mariovaldez.nextiatestapp.data.remote.model.Benevit
import java.io.Serializable

data class BenevitResponse(
    var locked : List<Benevit>,
    var unlocked : List<Benevit>
):Serializable
