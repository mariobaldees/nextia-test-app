package com.mariovaldez.nextiatestapp.data.remote.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class User(

    @SerializedName("email")
    private val email: String,

    @SerializedName("password")
    private val password:String
):Serializable