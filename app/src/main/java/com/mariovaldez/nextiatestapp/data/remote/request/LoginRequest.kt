package com.mariovaldez.nextiatestapp.data.remote.request

import com.google.gson.annotations.SerializedName
import com.mariovaldez.nextiatestapp.data.remote.model.User
import java.io.Serializable

data class LoginRequest(
    @SerializedName("user")
    private val user: User
)

