package com.mariovaldez.nextiatestapp.data.remote.model

import java.io.Serializable

data class Benevit(
    val id: Int,
    val name: String,
    val description: String,
    val title: String,
    val instructions: String,
    val expiration_date: String,
    val active:Boolean,
    val primary_color: String,
    val has_coupons:Boolean,
    val vector_file_name: String,
    val vector_full_path: String,
    val slug: String,
    val wallet: Wallet,
    val territories: List<Territories>,
    val ally: Ally,
    val is_available_in_all_territories: Boolean,
    val is_available_in_ecommerce: Boolean,
    val is_available_in_physical_store: Boolean,
    var isLocked : Boolean,
    var duration : String
):Serializable
