package com.mariovaldez.nextiatestapp.data.network

import com.mariovaldez.nextiatestapp.data.remote.request.LoginRequest
import com.mariovaldez.nextiatestapp.data.remote.response.BenevitResponse
import com.mariovaldez.nextiatestapp.data.remote.response.LoginResponse
import com.mariovaldez.nextiatestapp.data.remote.response.WalletResponse
import retrofit2.Call
import retrofit2.http.*
import javax.security.auth.callback.Callback

interface NextiaAPI {

    @POST("login")
    fun login(
        @Body request:LoginRequest
    ): Call<LoginResponse>

    @GET("member/wallets")
    fun getWallets():Call<List<WalletResponse>>

    @GET("member/landing_benevits")
    fun getBenevits(
        @Header("Authorization") auth:String
    ):Call<BenevitResponse>

}