package com.mariovaldez.nextiatestapp.data.remote.model

data class Ally(
    val id: Int,
    val name: String,
    val slug: String,
    val mini_logo_file_name: String,
    val mini_logo_full_path: String,
    val description: String

)
