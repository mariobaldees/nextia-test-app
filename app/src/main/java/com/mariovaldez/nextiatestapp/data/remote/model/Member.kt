package com.mariovaldez.nextiatestapp.data.remote.model

data class Member(
    private val id: Int,
    private val user_id: Int,
    private val id_socio_infonavit: String,
    private val name : String,
    private val lastnam: String,
    private val mobile: String,
    private val zipcode: Int,
    private val avatar: String,
    private val subscribed_to_newsletter : Boolean
)
