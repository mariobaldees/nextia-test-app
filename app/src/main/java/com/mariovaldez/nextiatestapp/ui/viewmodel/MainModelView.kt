package com.mariovaldez.nextiatestapp.ui.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mariovaldez.nextiatestapp.repository.LoginRepository
import com.mariovaldez.nextiatestapp.repository.MainRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainModelView @Inject constructor(
    private val repository: MainRepository,
): ViewModel() {

    private val _signOut= MutableLiveData<Boolean>()
    val signOut: LiveData<Boolean>
        get() = _signOut

    fun signOut() {
        viewModelScope.launch {
            repository.signout().let {
                println(it)
                _signOut.postValue(true)
            }
        }
    }
}