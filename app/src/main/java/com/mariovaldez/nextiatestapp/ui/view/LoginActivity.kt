package com.mariovaldez.nextiatestapp.ui.view

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Paint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.Html
import android.text.TextWatcher
import android.view.KeyEvent
import androidx.activity.viewModels
import com.mariovaldez.nextiatestapp.R
import com.mariovaldez.nextiatestapp.data.remote.model.User
import com.mariovaldez.nextiatestapp.data.remote.request.LoginRequest
import com.mariovaldez.nextiatestapp.databinding.ActivityLoginBinding
import com.mariovaldez.nextiatestapp.ui.viewmodel.LoginViewModel
import dagger.hilt.android.AndroidEntryPoint
import androidx.core.content.ContextCompat
import com.google.gson.Gson
import android.content.DialogInterface
import android.content.Intent
import android.view.View


@AndroidEntryPoint
class LoginActivity : AppCompatActivity() {
    private lateinit var binding: ActivityLoginBinding
    private val loginViewModel: LoginViewModel by viewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        start()

    }

    fun start(){
        binding.forgotPassword.setPaintFlags(binding.forgotPassword.getPaintFlags() or Paint.UNDERLINE_TEXT_FLAG)
        binding.forgotPassword.text = "¿Olvidó su contraseña?"
        binding.register.text = Html.fromHtml("No tengo cuenta <u>Registrarme</u>")
        setUpListeners()
        setupObservers()
    }

    fun setUpListeners(){
        binding.inputPasswordLogin.setOnKeyListener(object: View.OnKeyListener {
            override fun onKey(v: View?, keyCode: Int, event: KeyEvent?): Boolean {
                if(event!!.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER){
                    checkEditText()
                    return true
                }
                return false
            }
        })

        binding.btnLogin.setOnClickListener {
            checkEditText()
        }
        binding.inputPasswordLogin.addTextChangedListener(object : TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if(s.toString().trim().isNotEmpty()){
                    setEnableButton(true)
                }else{
                    setEnableButton(false)
                }
            }

            override fun afterTextChanged(s: Editable?) {

            }

        })
    }
    fun setupObservers(){
        loginViewModel.loginAuth.observe(this,{
            if(it != null){
                println("AUTH LOGINACTIVITY = $it")
  //              setupSharedPreferences(it)
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
                this.finish()
            }
        })

        loginViewModel.loginResponse.observe(this,{ response->
            showAlert(getString(R.string.error_pass_or_email))
        })

        loginViewModel.loginResponseError.observe(this,{
            showAlert(getString(R.string.NETWORKERROR))
        })
    }

    fun showAlert(message:String){
        val dlgAlert: android.app.AlertDialog.Builder = android.app.AlertDialog.Builder(this)

        dlgAlert.setMessage("Intente de nuevo")
        dlgAlert.setTitle(message)
        dlgAlert.setPositiveButton("OK", null)
        dlgAlert.setCancelable(true)
        dlgAlert.create().show()

        dlgAlert.setPositiveButton("Ok",
            { dialog, which ->dialog.cancel() })
    }


    fun setEnableButton(boolean: Boolean){
        binding.btnLogin.isEnabled = boolean
        var color:ColorStateList?
        if(boolean){
            color =  ContextCompat.getColorStateList(applicationContext,R.color.red)

        }else{
            color =  ContextCompat.getColorStateList(applicationContext,R.color.gray)
        }
        binding.btnLogin.backgroundTintList = color
    }

    override fun onKeyUp(keyCode: Int, event: KeyEvent?): Boolean {
            return when (keyCode){
                KeyEvent.ACTION_DOWN ->{
                    println("onKeyUp")
                    if(binding.inputPasswordLogin.isSelected){
                        checkEditText()
                    }
                    true
                }
                else -> super.onKeyUp(keyCode, event)
            }
    }



    private fun checkEditText(){
        if(binding.inputEmailLogin.text!!.isNotEmpty() && binding.inputPasswordLogin.text!!.isNotEmpty()){
            val email = binding.inputEmailLogin.text.toString()
            val password = binding.inputPasswordLogin.text.toString()
            val user = User(email,password)
            val loginRequest = LoginRequest(user)
            val json = Gson().toJson(loginRequest)
            println(json)
            loginViewModel.login(loginRequest)
        }
    }
}