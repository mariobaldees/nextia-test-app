package com.mariovaldez.nextiatestapp.ui.viewmodel

import android.app.Application
import android.content.Context
import androidx.lifecycle.*
import com.mariovaldez.nextiatestapp.data.local.NextiaItem
import com.mariovaldez.nextiatestapp.data.network.NextiaAPI
import com.mariovaldez.nextiatestapp.data.remote.request.LoginRequest
import com.mariovaldez.nextiatestapp.data.remote.response.LoginResponse
import com.mariovaldez.nextiatestapp.repository.LoginRepository
import dagger.hilt.android.internal.Contexts
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
    private val repository: LoginRepository,
): ViewModel() {

    private val _loginResponse= MutableLiveData<LoginResponse>()
    val loginResponse: LiveData<LoginResponse>
        get() = _loginResponse

    private val _loginResponseError= MutableLiveData<Boolean>()
    val loginResponseError: LiveData<Boolean>
        get() = _loginResponseError



    private val _loginAuth= MutableLiveData<String>()
    val loginAuth: LiveData<String>
        get() = _loginAuth

    lateinit var responseLogin: LoginResponse
    fun login(login: LoginRequest){
        repository.login(login).enqueue(object: Callback<LoginResponse>{
            override fun onResponse(call: Call<LoginResponse>, response: Response<LoginResponse>) {
                if(response.isSuccessful){
                    val auth: String? = response.headers()["Authorization"]
                    responseLogin = response.body()!!
                    println(response.body())
                    insertAuth(auth!!)
                   // _loginResponse.postValue(response.body())
                }else{
                    with(_loginResponse) { postValue(null) }
                }
            }

            override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                println(t.message)
                with(_loginResponseError) { postValue(null) }
            }

        })


    }

    private fun insertAuth(auth:String) {
        val item  = NextiaItem(1,auth)
        viewModelScope.launch {
            repository.insertAuth(item)
            _loginAuth.postValue(auth)
        }

    }


}