package com.mariovaldez.nextiatestapp.ui.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.SystemClock
import com.mariovaldez.nextiatestapp.R
import com.mariovaldez.nextiatestapp.databinding.ActivitySplashBinding
import dagger.hilt.android.AndroidEntryPoint

import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.content.Intent
import android.os.CountDownTimer


@AndroidEntryPoint
class SplashActivity : AppCompatActivity() {
    private lateinit var binding : ActivitySplashBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySplashBinding.inflate(layoutInflater)
        val view = binding.root

        start()
        setContentView(view)
    }

    fun start(){
        val fadeIn: Animation = AnimationUtils.loadAnimation(this, R.anim.fade_in)
        binding.logo.startAnimation(fadeIn)

        val timer = object: CountDownTimer(3000, 1000) {
            override fun onTick(millisUntilFinished: Long) {}
            override fun onFinish() {exit()}
        }
        timer.start()
    }

    fun exit(){
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
        this.finish()
    }
}