package com.mariovaldez.nextiatestapp.ui.view

import android.content.Context
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.mariovaldez.nextiatestapp.R
import com.mariovaldez.nextiatestapp.adapter.WalletAdapter
import com.mariovaldez.nextiatestapp.data.remote.response.WalletResponse
import com.mariovaldez.nextiatestapp.databinding.BenevitsFragmentBinding
import com.mariovaldez.nextiatestapp.ui.viewmodel.BenevitsViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class BenevitsFragment : Fragment() {

    companion object {
        fun newInstance() = BenevitsFragment()
    }

    lateinit var binding: BenevitsFragmentBinding
    private lateinit var viewModel: BenevitsViewModel

    private lateinit var adapterWallet: WalletAdapter
    lateinit var walletResponse: List<WalletResponse>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = BenevitsFragmentBinding.inflate(inflater,container,false)
        val view = binding.root
        return view
    }

    private fun start() {
        setupAdapter()
        viewModel.getAuth()
        binding.shimmerLayout.startShimmerAnimation()

        viewModel.walletResponse.observe(viewLifecycleOwner,{
            binding.shimmerLayout.stopShimmerAnimation()
            binding.shimmerLayout.visibility = View.GONE
            adapterWallet.walletItems = it
        })
    }

    private fun setupAdapter() {
        adapterWallet = WalletAdapter()
        binding.rvBenevits.apply {
            adapter = adapterWallet
            adapter!!.notifyDataSetChanged()
            layoutManager = LinearLayoutManager(activity,LinearLayoutManager.VERTICAL,false)
            setHasFixedSize(true)
        }

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(BenevitsViewModel::class.java)
        start()
    }


}