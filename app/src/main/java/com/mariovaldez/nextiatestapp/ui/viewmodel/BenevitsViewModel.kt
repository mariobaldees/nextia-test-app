package com.mariovaldez.nextiatestapp.ui.viewmodel

import android.content.Context
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mariovaldez.nextiatestapp.data.remote.model.Benevit
import com.mariovaldez.nextiatestapp.data.remote.response.BenevitResponse
import com.mariovaldez.nextiatestapp.data.remote.response.WalletResponse
import com.mariovaldez.nextiatestapp.repository.BenevitRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.Period
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit
import java.util.*
import java.util.concurrent.TimeUnit
import java.util.function.Consumer
import javax.inject.Inject

@HiltViewModel
class BenevitsViewModel @Inject() constructor(
        private val repository: BenevitRepository
    ): ViewModel() {

    private val _walletResponse= MutableLiveData<List<WalletResponse>>()
    val walletResponse: LiveData<List<WalletResponse>>
        get() = _walletResponse
    private lateinit var wallets: List<WalletResponse>
    private lateinit var walletsResponse: List<WalletResponse>
    private lateinit var benevitResponse: BenevitResponse


   lateinit var auth:String


    fun getWallets(){
        repository.getWallets().enqueue(object :Callback<List<WalletResponse>>{
            override fun onResponse(
                call: Call<List<WalletResponse>>,
                response: Response<List<WalletResponse>>
            ) {
                if(response.isSuccessful){
                    wallets = response.body()!!
                    println(wallets)
                    println(auth)
                    getBenevits()

                }
                println(response.body())
            }

            override fun onFailure(call: Call<List<WalletResponse>>, t: Throwable) {
               println(t.message)
            }


        })
    }

    fun getBenevits(){
        repository.getBenevits(auth).enqueue(object : Callback<BenevitResponse>{
            override fun onResponse(
                call: Call<BenevitResponse>,
                response: Response<BenevitResponse>
            ) {
                if(response.isSuccessful){
                    benevitResponse = response.body()!!
                    setWalletsBenevit()
                }else{
                    println(response.message())
                }

            }

            override fun onFailure(call: Call<BenevitResponse>, t: Throwable) {
                println(t.message)
            }

        })
    }
    fun setWalletsBenevit() {
        benevitResponse.locked = benevitResponse.locked.sortedBy { it.wallet.id }
        benevitResponse.unlocked = benevitResponse.unlocked.sortedBy { it.wallet.id }

        wallets.forEach{wallet->
            var benevits = mutableListOf<Benevit>()
            benevitResponse.locked.forEach{ locked->
                if(locked.wallet.id == wallet.id){
                    locked.isLocked = true
                    benevits.add(locked)
                }
            }
            benevitResponse.unlocked.forEach{ unlocked->
                if(unlocked.wallet.id == wallet.id){
                    unlocked.isLocked = false
                    unlocked.duration = getDaysFromPromotion(unlocked.expiration_date)
                    benevits.add(unlocked)
                }
            }
            benevits.shuffle()

            wallet.benevits = benevits
        }

        walletsResponse = wallets
        println(walletsResponse)
        _walletResponse.postValue(wallets)

    }


    fun getDaysFromPromotion(dateString:String):String{
        val datenow = Date()
        val sdf = SimpleDateFormat("yyyy-MM-dd")
        val date: Date = sdf.parse(dateString)

        val diff: Long = date.getTime() - datenow.getTime()
        val days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS)
        val string = "Vence en $days días"
        return string

    }

    fun getAuth(){
        viewModelScope.launch {
            repository.getAuth().let {
                auth = it.auth
                println("view model auth = $auth")
                getWallets()
            }
        }
    }


}