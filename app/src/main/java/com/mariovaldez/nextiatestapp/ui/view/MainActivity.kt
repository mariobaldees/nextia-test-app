package com.mariovaldez.nextiatestapp.ui.view

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.activity.viewModels
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.navigation.NavigationView
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.drawerlayout.widget.DrawerLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import com.mariovaldez.nextiatestapp.R
import com.mariovaldez.nextiatestapp.databinding.ActivityMainBinding
import com.mariovaldez.nextiatestapp.ui.viewmodel.MainModelView
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivityMainBinding
    private lateinit var navController: NavController
    private val viewModel : MainModelView by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setupListeners()
        setupAppbar()


    }

    private fun setupListeners() {
        viewModel.signOut.observe(this,{
            if(it){
                signOut()
            }
        })
    }

    private fun setupAppbar() {
        setSupportActionBar(binding.appBarMain.toolbar)

        val drawerLayout: DrawerLayout = binding.drawerLayout
        val navView: NavigationView = binding.navView
        val navHostFragment = supportFragmentManager.findFragmentById(R.id.nav_host_fragment_content_main) as NavHostFragment
        navController = navHostFragment.navController
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_fr_benevits
            ), drawerLayout
        )
        title = ""
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

        navView.setNavigationItemSelectedListener { menuItem: MenuItem ->
            changeNavigation(menuItem.itemId)
            drawerLayout.closeDrawer(GravityCompat.START)
            true
        }
    }

    private fun changeNavigation(itemId: Int) {
        when(itemId){
            R.id.nav_menu_benevits->{
                navController.navigate(R.id.nav_fr_benevits)
            }
            R.id.sign_out->{
                showDialog()
            }
        }
    }

    private fun showDialog() {
        val dlgAlert: android.app.AlertDialog.Builder = android.app.AlertDialog.Builder(this)

        dlgAlert.setMessage("Cerrar Sesión")
        dlgAlert.setTitle("¿ Está seguro que desea cerrar sesión ?")
        dlgAlert.setCancelable(true)
        dlgAlert.setPositiveButton("SÍ") { dialog, which ->
            viewModel.signOut()
            dialog.cancel() }
        dlgAlert.setNegativeButton("NO") { dialog, which -> dialog.cancel() }
        dlgAlert.create().show()


    }

    private fun signOut() {
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
        this.finish()
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment_content_main)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }
}