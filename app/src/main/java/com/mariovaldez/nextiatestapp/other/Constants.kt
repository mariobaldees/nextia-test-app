package com.mariovaldez.nextiatestapp.other

object Constants {
    const val DATABASE_NAME = "user_auth"
    const val URL = "https://staging.api.socioinfonavit.io/api/v1/"
}